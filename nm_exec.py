import os
import time
import threading
import tempfile
import numpy as np
import shutil

def time_domain_conversion(file_name, N):
    result = np.loadtxt(file_name)
    pres = result[:, 0] + 1j * result[:, 1]
    df = result[1, 2] - result[0, 2]
    f0 = result[0, 2]
    fN = result[-1, 2]
    freq = np.arange(f0, fN + df, df)
    pre_freq = np.arange(df, f0, df)
    pre_pres = np.zeros_like(pre_freq)
    freq = np.hstack((pre_freq, freq))
    post_freq = np.arange(fN + df, N * df + df, df)
    post_pres = np.zeros_like(post_freq)
    freq = np.hstack((freq, post_freq))
    pres = np.hstack((pre_pres, pres, post_pres))
    signal = np.real(np.fft.ifft(pres, N))
    signal2 = signal
    signal3 = (signal2 - np.mean(signal2)) / np.max(np.abs(signal2))
    # signal3 = signal3[::-1]
    return signal3[range(N)]

def node_os_system(com):
    os.system(com)

def nm_exec(program='mode1',
            template='data',
            tfunc=None,
            values=None,
            N=1024,
            tmpdir='/tmp',
            nodes={'localhost': 4},
            resdir='/res',
            get_signals=False,
            verbose=True,
            timeout=100):
    tic = time.time()
    tmpd = tempfile.mkdtemp(dir=tmpdir)
    cores_per_node = list(nodes.values())
    hostnames = list(nodes.keys())
    cum_cores = np.cumsum(cores_per_node)
    num_cores = cum_cores[-1]
    if values is None:
        values_shape = [1]
    else:
        values_shape = values.shape
    exec_bank = {}
    mode1_arg = program
    for host in hostnames:
        exec_bank[host] = [mode1_arg + ' :::']
    for i in range(values_shape[0]):
        mod = i % num_cores
        # find the id of the node
        n = next(x[0] for x in enumerate(cum_cores) if x[1] > mod)
        file_number = str(i).zfill(5)
        c = 'run_' + file_number
        exec_bank[hostnames[n]].append(c)
        run_i = open(tmpd + '/' + c, 'w')
        if tfunc is None:
            run_i.write(template)
        else:
            run_i.write(template % tfunc(values[i, :]))
        run_i.close()
    processes = []
    for n, host in enumerate(hostnames):
        mode1_exec = str.join(' ', exec_bank[host])
        if len(nodes) == 1 and list(nodes.keys())[0] == 'localhost':
            com = 'cd ' + tmpd + ' ; parallel -j' \
              + str(cores_per_node[n]) + ' nohup ' + mode1_exec + \
              ' ::: '
            if verbose:
                com += ' > screen.txt 2>&1'
            else:
                com += ' > /dev/null 2>&1'
        else:
            com = 'ssh ' + host + ' \' cd ' + tmpd + ' ; parallel -j' \
              + str(cores_per_node[n]) + ' nohup ' + mode1_exec + \
              ' ::: > /dev/null 2>&1 \' '
        processes.append(threading.Thread(target=node_os_system, args=(com,)))
    for p in processes:
        p.start()
    for p in processes:
        p.join(timeout=timeout)
    toc = time.time()
    signals = []
    for i in range(values_shape[0]):
        file_number = str(i).zfill(5)
        if get_signals:
            c = tmpd + '/run_' + file_number + '.fft'
            signals.append(time_domain_conversion(c, N))
    if resdir is not None:
        os.system('cp -rf ' + tmpd + ' ' + resdir)
    shutil.rmtree(tmpd)
    if get_signals:
        print('execution time:', int(toc - tic), 'seconds')
        return np.array(signals)
    print('execution time:', int(toc - tic), 'seconds')
