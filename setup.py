from setuptools import setup

setup(name='kesmarag-waveprop',
      version='0.0.3',
      description='wave propagation',
      author='Costas Smaragdakis',
      author_email='kesmarag@gmail.com',
      url='https://gitlab.com/kesmarag/waveprop',
      packages=['kesmarag.waveprop'],
      package_dir={'kesmarag.waveprop': './'},
      install_requires=['numpy'], )
